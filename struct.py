from collections import deque

class stack :
    ###The list methods make it very easy to use a list as a stack,
    #  where the last element added
    #  is the first element retrieved (“last-in, first-out”).
    ############
    ###To add an item to the top of the stack, use append().
    ##############
    ###To retrieve an item from the top of the stack,
    #  use pop() without an explicit index.
    #######
    def __init__(self):
        self.__storage = []


    def pop_stack(self):
        return self.__storage.pop()

    def push_stack(self,p):
        self.__storage.append(p)


class queue :
    ###It is also possible to use a list as a queue, where
#  the first element added is the first element retrieved (“first-in, first-out”);
    #########################

    ###lists are not efficient for this purpose.
# While appends and pops from the end of list are fast,
#  doing inserts or pops from the beginning of a list is slow
# (because all of the other elements have to be shifted by one).
    def __init__(self):
        self.__queue = deque([])

    def push_queue(self,add):
        self.__queue.append(add)

    def pop_queue(self):
        try:
            
        return self.__queue.popleft()

